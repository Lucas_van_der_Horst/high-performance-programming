import numpy as np
import time
import math
from mpi4py import MPI
import multiprocessing as mp
import sys
from typing import List

comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()


def calculate_sieves(n: int) -> List[int]:
    """
    Uses Sieve of Eratosthenes to find all prime numbers up to a given limit.
    The resulting list can be used to sieve an array to n^2.

    :param n: the limit
    :return: a list of primes
    """
    d_array = np.full(n-2, True)
    k = 2
    for _ in range(k, math.ceil(math.sqrt(n))):
        d_array[k*k-2::k] = False
        k += np.argmax(d_array[k-1:]) + 1
    return d_array.nonzero()[0] + 2


def sieve_block(begin: int, end: int, sieves: List[int]) -> int:
    """
    Sieve a block between a given begin and end with the given sieves.

    :param begin: the first number of the block
    :param end: the end of the block, the number itself isn't included in the block
    :param sieves: a list of primes used to sieve the block
    :return: the number of primes found in the block
    """
    d_array = np.full(end-begin, True)

    # Tried for way to long to parallelize this loop, using both shared memory and parallel reduce,
    # but it wasn't performance beneficial
    for k in sieves:
        first_index = math.ceil(begin / k) * k - begin
        d_array[first_index::k] = False

    return np.count_nonzero(d_array)


if __name__ == "__main__":
    # Get parameters
    total_size = int(sys.argv[1])
    n_processes = int(sys.argv[2])

    # Start timer
    start_time = time.time()

    # Calculate the prime numbers that are going to be used to sieve the blocks
    sieves_size = math.ceil(math.sqrt(total_size))
    sieves = calculate_sieves(sieves_size) if rank == 0 else None
    sieves = comm.bcast(sieves, root=0)

    # Determine this process' block by using it's rank
    begin_index = math.ceil(rank / size * (total_size - sieves_size)) + sieves_size
    end_index = math.ceil((rank+1) / size * (total_size - sieves_size)) + sieves_size

    # Chop this block up into smaller parts for multiprocessing
    sub_blocks = list(map(int, np.linspace(begin_index, end_index, n_processes + 1)))
    pool = mp.Pool(n_processes)
    process_parameters = [(sub_blocks[i], sub_blocks[i + 1], sieves) for i in range(n_processes)]
    # Sieve the blocks
    found_primes = sum(pool.starmap(sieve_block, process_parameters))
    pool.close()
    pool.join()

    # Sum the number of found primes to one number
    prime_sum = comm.reduce(found_primes, op=MPI.SUM)

    # End the timer and show the results
    if rank == 0:
        end_time = time.time()
        print(f'Found a total of {prime_sum + len(sieves)} primes')
        print(f"in {end_time - start_time} seconds")
