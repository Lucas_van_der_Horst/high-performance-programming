"""
This module adds the CT (Comparable Type),
Objects of this type are comparable (duh),
because this was unfortunately not built-in.
Source:
https://stackoverflow.com/a/37669538
"""

from abc import ABCMeta, abstractmethod
from typing import Any, TypeVar


class Comparable(metaclass=ABCMeta):
    @abstractmethod
    def __lt__(self, other: Any) -> bool: ...


CT = TypeVar('CT', bound=Comparable)
