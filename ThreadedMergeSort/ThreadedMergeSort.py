from multiprocessing.pool import ThreadPool
from typing import Sequence, List
from ComparableType import CT


def merge_lists(list_a: List[CT], list_b: List[CT]) -> List[CT]:
    """Merges the two given sorted lists into a new sorted list"""
    len_a, len_b = len(list_a), len(list_b)
    i_a, i_b = 0, 0
    result = []
    while i_a < len_a and i_b < len_b:
        if list_a[i_a] < list_b[i_b]:
            result.append(list_a[i_a])
            i_a += 1
        else:
            result.append(list_b[i_b])
            i_b += 1
    return result + list_a[i_a:] + list_b[i_b:]


def threaded_merge_sort(input_array: Sequence[CT]) -> Sequence[CT]:
    """Merge sorts the given array using threads"""
    working_array = [[x] for x in input_array]

    # Do in parallel with threads
    pool = ThreadPool()
    while len(working_array) > 3:
        single = [working_array.pop()] if len(working_array) % 2 == 1 else []
        pairs = zip(working_array[::2], working_array[1::2])
        working_array = pool.map(lambda x: merge_lists(*x), pairs) + single
    pool.close()
    pool.join()

    # The last step(s) don't benefit from threads
    while len(working_array) > 1:
        single = [working_array.pop()] if len(working_array) % 2 == 1 else []
        working_array = [merge_lists(*working_array)] + single

    return working_array[0]


def time_plot(length_range):
    import numpy as np
    import time
    import matplotlib.pyplot as plt
    x, y = [], []
    for n in length_range:
        x.append(1.5 ** n)
        random_array = np.random.rand(int(x[-1]))
        start = time.time()
        threaded_merge_sort(random_array)
        y.append(time.time() - start)
    plt.plot(x, y)
    plt.xlabel('Array length')
    plt.ylabel('Time duration (in seconds)')
    plt.title('Time complexity benchmark threaded merge sort')
    plt.show()


if __name__ == "__main__":
    time_plot(range(1, 30))
